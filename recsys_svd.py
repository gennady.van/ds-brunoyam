import pandas as pd
import numpy as np



class RecSysSVD:
    
    def __init__(self):
        self.temp_rating = 3
        
        
        
    def fit(self, X, y):
        
        data = X.copy()
        data['rating'] = y
        
        self.users = list(set(data.userId))
        self.users_dict = {self.users[i]:i for i in range(len(self.users))}

        self.movies = list(set(data.movieId))
        self.movies_dict = {self.movies[i]:i for i in range(len(self.movies))}

        df_pivot_table = pd.pivot_table(data, index = 'userId', columns = 'movieId', values = 'rating')
        df_pivot_table = df_pivot_table.fillna(0)
        pivot_table = np.array(df_pivot_table)

        u, s, vh = np.linalg.svd(pivot_table, full_matrices=False)
        s_diag = np.diag(s)
        self.result_table = np.dot(u, np.dot(s_diag, vh))
    
    
    
    def predict(self, X):
        
        pred_rating = []
        for i in X.values:
            temp_rating = self.predict_one(i[0], i[1])
            pred_rating.append(temp_rating)
        
        return pred_rating
        
        
        
    def predict_one(self, user, movie):
        
        if user in self.users and movie in self.movies: 
            return self.result_table[self.users_dict[user]][self.movies_dict[movie]]
        else:
            return self.temp_rating