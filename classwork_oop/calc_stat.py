import math

def mean(lst):
    
    S = 0
    L = len(lst)
    for i in lst:
        S += i
    return S/L




def var(lst):

    S = 0
    L = len(lst)
    M = mean(lst)
    for i in lst:
        S += (i-M)**2
    return S/L




def std(lst):
    
    return math.sqrt(var(lst))
    