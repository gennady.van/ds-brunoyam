import math
from class_mean import ClassMean

#дописать skewness
#дописать kurtosis
#std вынести в базовый класс
#инкапсулировать методы skewness, kurtosis
#можно ли инкапсулировать атрибуты lst, L и почему?
#создать модуль get_param с функциями mean, std и использовать их в базовом классе


class CalcStat(ClassMean):
    
    
    def __init__(self, spisok):
        self.lst = spisok
        self.L = len(spisok)
        self.M = self.mean()
        self.variance = self.__var()
        self.sigma = self.__std()
    
    

#    def __mean(self):
#        S = 0
#        for i in self.lst:
#            S += i
#        return S/self.L

    
    
    def __var(self):
        S = 0
        M = self.M
        for i in self.lst:
            S += (i-M)**2
        return S/self.L
    
    
    
    def __std(self):
        return math.sqrt(self.variance)
    
    
