import pandas as pd
import numpy as np
from surprise import NormalPredictor
from surprise import Dataset
from surprise import Reader
from surprise.model_selection import cross_validate

from surprise import SVD
from surprise import Dataset
from surprise.model_selection import cross_validate



class SurpriceSVD:
    
    def __init__(self):
        self.temp_rating = 3
        self.algo = SVD()
        
        
        
    def fit(self, X, y):
        
        data = X.copy()
        data['rating'] = y
        
        self.users = list(set(data.userId))
        self.users_dict = {self.users[i]:i for i in range(len(self.users))}

        self.movies = list(set(data.movieId))
        self.movies_dict = {self.movies[i]:i for i in range(len(self.movies))}
        
        data = data.rename({'userId': 'userID','movieId': 'itemID'}, axis = 1)
        data = data[['itemID', 'userID', 'rating']]
        
        reader = Reader(rating_scale=(1, 5))
        data = Dataset.load_from_df(data[['userID', 'itemID', 'rating']], reader)
        trainset = data.build_full_trainset()
        
        self.algo.fit(trainset)
 

    
    def predict(self, X):
        
        pred_rating = []
        for i in X.values:
            temp_rating = self.predict_one(i[0], i[1])
            pred_rating.append(temp_rating)
        
        return pred_rating
        
        
        
    def predict_one(self, user, movie):
        
        if user in self.users and movie in self.movies: 
            
            uid = self.users_dict[user]
            iid = self.movies_dict[movie]
            pred = self.algo.predict(uid, iid, r_ui=0, verbose=False)
            return pred.est
        else:
            return self.temp_rating